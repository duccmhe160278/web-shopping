
import java.util.ArrayList;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author HP
 */
public class MyGeneric {

    public ArrayList<Object> myGeneric(ArrayList<Object> list, int page, int elements) {
        ArrayList<Object> listpage = new ArrayList<>();
        int start = page * elements - elements - 1;
        for (int i = 0; i < elements; i++) {
            listpage.add(list.get(start+i));
        }
        return listpage;
    }
}
