/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.Category;
import dal.Product;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import models.CategoryDAO;
import models.ProductDAO;

/**
 *
 * @author HP
 */
public class CategoryController extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Category> categories = new CategoryDAO().getAllCategory();
        int cid = Integer.parseInt(req.getParameter("cid"));
        Category cat = new CategoryDAO().getCategoryById(cid);
        ArrayList<Product> list = new ProductDAO().getProductsByCategory(cid);
        
        req.setAttribute("categories", categories);
        req.setAttribute("cat", cat);
        req.setAttribute("list", list);
        
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
    
}
