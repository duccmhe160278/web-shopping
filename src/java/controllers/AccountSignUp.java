package controllers;

import dal.Account;
import dal.Customer;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import models.AccountDAO;

public class AccountSignUp extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("AccSession") != null) {
            resp.sendRedirect("../index.jsp");
        } else {
            req.getRequestDispatcher("../signup.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String CompanyName = req.getParameter("txtCompanyName");
        String ContactName = req.getParameter("txtContactName");
        String ContactTitle = req.getParameter("txtContactTitle");
        String Address = req.getParameter("txtAddress");
        String Email = req.getParameter("txtEmail");
        String Password = req.getParameter("txtPass");
        String RePassword = req.getParameter("txtRePass");

        boolean flag = true;

        if (CompanyName.isEmpty()) {
            flag = false;
            req.setAttribute("errorCompanyName", "Company name is required");
        }
        if (CompanyName.isEmpty()) {
            flag = false;
            req.setAttribute("errorCompanyName", "Company name is required");
        }
        if (ContactName.isEmpty()) {
            flag = false;
            req.setAttribute("errorContactName", "Contact name is required");
        }
        if (ContactTitle.isEmpty()) {
            flag = false;
            req.setAttribute("errorContactTitle", "Contact title is required");
        }
        if (Address.isEmpty()) {
            flag = false;
            req.setAttribute("errorAddress", "Address is required");
        }
        if (Email.isEmpty()) {
            flag = false;
            req.setAttribute("errorEmail", "Email is required");
        }
        if (Password.isEmpty()) {
            flag = false;
            req.setAttribute("errorPassword", "Password is required");
        }
        if (RePassword.isEmpty()) {
            flag = false;
            req.setAttribute("errorRePassword", "Re-Password is required");
        } else {
            if (!Password.equals(RePassword)) {
                flag = false;
                req.setAttribute("errorRePassword", "Password and Re-Password are not the same");
            }
        }

        if (flag) {
            Customer cust = new Customer("", CompanyName, ContactName, ContactTitle, Address);
            Account acc = new Account(0, Email, Password, "", 0, 0);
            if (new AccountDAO().createAccount(cust, acc) > 0) {
                resp.sendRedirect("signin");
            } else {
                req.getRequestDispatcher("../signup.jsp").forward(req, resp);
            }
        } else {
            req.getRequestDispatcher("../signup.jsp").forward(req, resp);
        }

    }
}
