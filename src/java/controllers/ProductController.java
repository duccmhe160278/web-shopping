/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import dal.Category;
import dal.Product;
import models.CategoryDAO;
import models.ProductDAO;

/**
 *
 * @author HP
 */
public class ProductController extends HttpServlet {
//    chua nhieu chuc nang

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Category> categories = new CategoryDAO().getAllCategory();
        ArrayList<Product> hot = new ProductDAO().getHotProduct();
        ArrayList<Product> bestSale = new ProductDAO().getBestSaleProduct();
        ArrayList<Product> newP = new ProductDAO().getNewProduct();
        
        req.setAttribute("categories", categories);
        req.setAttribute("hot", hot);
        req.setAttribute("bestSale", bestSale);
        req.setAttribute("newP", newP);
        
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

}
