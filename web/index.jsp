
<%@include file="template/header.jsp" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}"/>

<div id="content-left">
    <h3>CATEGORY</h3>
    <ul>
        <c:forEach items="${categories}" var="i">
            <a href="${path}/category?cid=${i.getCategoryId()}"><li>${i.getCategoryName()}</li></a>
                </c:forEach>
    </ul>
</div>

<div id="content-right">

    <c:if test="${hot != null}">
        <!--san pham da duoc mua va co discount tot nhat-->
        <div class="path">Hot</b></div>
        <div class="content-main">
            <c:forEach items="${hot}" var="i">
                <div class="product">
                    <a href=""><img src="${path}/img/1.jpg" width="100%"/></a>
                    <div class="name"><a href="detail?pid=${i.getProductID()}">${i.getProductName()}</a></div>
                    <div class="price">$ ${i.getUnitPrice()}</div>
                    <div><a href="${path}/cart?type=buy&pid=${i.getProductID()}">Buy now</a></div>
                </div>
            </c:forEach>
        </div>
    </c:if>

    <c:if test="${bestSale != null}">
        <!--san pham nhieu khach hang mua nhat-->
        <div class="path">Best Sale</b></div>
        <div class="content-main">
            <c:forEach items="${bestSale}" var="i">
                <div class="product">
                    <a href=""><img src="${path}/img/1.jpg" width="100%"/></a>
                    <div class="name"><a href="detail?pid=${i.getProductID()}">${i.getProductName()}</a></div>
                    <div class="price">$ ${i.getUnitPrice()}</div>
                    <div><a href="${path}/cart?type=buy&pid=${i.getProductID()}">Buy now</a></div>
                </div>
            </c:forEach>
        </div>
    </c:if>

    <c:if test="${newP != null}">
        <!--san pham moi-->
        <div class="path">New Product</b></div>
        <div class="content-main">
            <c:forEach items="${newP}" var="i">
                <div class="product">
                    <a href=""><img src="${path}/img/1.jpg" width="100%"/></a>
                    <div class="name"><a href="detail?pid=${i.getProductID()}">${i.getProductName()}</a></div>
                    <div class="price">$ ${i.getUnitPrice()}</div>
                    <div><a href="${path}/cart?type=buy&pid=${i.getProductID()}">Buy now</a></div>
                </div>
            </c:forEach>
        </div>
    </c:if>

    <c:if test="${list != null}">
        <div class="path">${cat.getCategoryName()}</b></div>
        <div class="content-main">
            <c:forEach items="${list}" var="i">
                <div class="product">
                    <a href=""><img src="${path}/img/1.jpg" width="100%"/></a>
                    <div class="name"><a href="detail?pid=${i.getProductID()}">${i.getProductName()}</a></div>
                    <div class="price">$ ${i.getUnitPrice()}</div>
                    <div><a href="${path}/cart?type=buy&pid=${i.getProductID()}">Buy now</a></div>
                </div>
            </c:forEach>
        </div>
    </c:if>
</div>


<%@include file="template/footer.jsp" %>
