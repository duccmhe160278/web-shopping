<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Index</title>
        <%
            String path = request.getContextPath();
        %>
        <link href="<%=path%>/css/style.css" rel="stylesheet"/>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <div id="logo">
                    <a href="<%=path%>/index"><img src="<%=path%>/img/logo.png"/></a>
                </div>
                <div id="banner">
                    <ul>
                        <li><a href="<%=path%>/cart.jsp">Cart: 0</a></li>
                        <%
                        if(session.getAttribute("AccSession")==null){
                        %>
                        <li><a href="<%=path%>/account/signin">SignIn</a></li>
                        <li><a href="<%=path%>/account/signup">SignUp</a></li>
                        <%
                            }else{
                        %>
                        <li><a href="<%=path%>/account/profile">Profile</a></li>
                        <li><a href="<%=path%>/account/signin">SignOut</a></li>
                        <%
                            }
                        %>
                    </ul>
                </div>
            </div>
            <div id="content">