<%@include file="template/header.jsp" %>
<c:set var="path" value="${pageContext.request.contextPath}"/>

<div id="content">
    <div id="content-left">
        <h3 style="font-weight: normal;">Welcome, ${AccSession[1].getContactName()}</h3>
        <h3>Account Management</h3>
        <ul>
            <a href="${path}/account/profile"><li>Personal information</li></a>
        </ul>
        <h3>My order</h3>
        <ul>
            <a href="profile1.html"><li>All orders</li></a>
            <a href="profile2.html"><li>Canceled order</li></a>
        </ul>
    </div>
    <div id="content-right">
        <div class="path">Personal information</b></div>
        <div class="content-main">
            <div id="profile-content">
                <div class="profile-content-col">
                    <div>Company name: <br/>${AccSession[1].getCompanyName()}</div>
                    <div>Contact name: <br/>${AccSession[1].getContactName()}</div>
                    <div>
                        <input type="submit" value="Edit info"/>
                    </div>
                </div>
                <div class="profile-content-col">
                    <div>Company title: <br/>${AccSession[1].getContactTitle()}</div>
                    <div>Address: <br/>${AccSession[1].getAddress()}</div>
                </div>
                <div class="profile-content-col">
                    <div>Email: <br/>${AccSession[0].getEmail()}</div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="template/footer.jsp" %>