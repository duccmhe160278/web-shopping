<%@include file="template/header.jsp" %>

<div id="form">
    <div id="form-title">
        <span><a href="<%=request.getContextPath()%>/account/signup" style="color: red;">SIGN UP</a></span>
        <span><a href="<%=request.getContextPath()%>/account/signin">SIGN IN</a></span>
    </div>
    <div id="form-content">
        <form method="post">
            <label>Company name<span style="color: red;">*</span></label><br/>
            <input type="text" name="txtCompanyName"/><br/>
            <span class="msg-error">${errorCompanyName}</span><br/>
            <label>Contact name<span style="color: red;">*</span></label><br/>
            <input type="text" name="txtContactName"/><br/>
            <span class="msg-error">${errorContactName}</span><br/>
            <label>Contact title<span style="color: red;">*</span></label><br/>
            <input type="text" name="txtContactTitle"/><br/>
            <span class="msg-error">${errorContactTitle}</span><br/>
            <label>Address<span style="color: red;">*</span></label><br/>
            <input type="text" name="txtAddress"/><br/>
            <span class="msg-error">${errorAddress}</span><br/>
            <label>Email<span style="color: red;">*</span></label><br/>
            <input type="text" name="txtEmail"/><br/>
            <span class="msg-error">${errorEmail}</span><br/>
            <label>Password<span style="color: red;">*</span></label><br/>
            <input type="password" name="txtPass"/><br/>
            <span class="msg-error">${errorPassword}</span><br/>
            <label>Re-Password<span style="color: red;">*</span></label><br/>
            <input type="password" name="txtRePass"/><br/>
            <span class="msg-error" name="txtRePass">${errorRePassword}</span><br/>
            <div></div>
            <input type="submit" value="SIGN UP" style="margin-bottom: 30px;"/>
        </form>
    </div>
</div>

<%@include file="template/footer.jsp" %>